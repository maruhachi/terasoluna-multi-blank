# Terasoluna multi blank で構築したてけとーなサンプル

## BookManagerを再現すべく以下の流れを実装

* Select
  * findAll, findAuthor(条件式)

* RepositoryはいまんとこMapperInterfaceでやってる
  * SQLが膨れたらMaperXMLへ移行する

## お試し起動

* MavenのPluginによりTomcat7で簡易確認が可能です
`# mvn clean tomcat7:run`

* PostgreSQLのデータベースを必要とします
  * url : localhost:5432
  * database : bookdb
  * table : book
  * user : postgres
  * password : postgres
