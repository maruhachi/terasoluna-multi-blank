package pink.nico.domain.model;

/**
 * @author ykonno
 * @since 2017/04/20
 *
 */
@lombok.Data
@lombok.Builder
public class Book {

    private String isbn;

    private String title;

    private String author;

    private String publisher;

}
