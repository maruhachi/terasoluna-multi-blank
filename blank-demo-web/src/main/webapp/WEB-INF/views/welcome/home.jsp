<div id="wrapper">
    <h1 id="title">Hello world!</h1>
    <p>The time on the server is ${serverTime}.</p>

    <h1>こっからデバッグ用</h1>
    <p>Message is ${message}</p>
    <hr />
    <h1>本の一覧</h1>
    <c:forEach var="book" items="${books}" varStatus="status">
        <p><bold>名前</bold> : ${book.publisher} , <bold>タイトル</bold> : ${book.title}</p>
    </c:forEach>
</div>
