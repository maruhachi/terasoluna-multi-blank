package pink.nico.domain.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pink.nico.domain.model.Book;
import pink.nico.domain.repository.BookRepository;

import javax.inject.Inject;
import java.util.List;

/**
 * @author ykonno
 * @since 2017/04/20
 */
@Service
@lombok.AllArgsConstructor
@Slf4j
public class BookServiceImpl implements BookService {

    @Inject
    BookRepository bookRepository;

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> findAuthor() {
        return bookRepository.findAuthor();
    }
}
