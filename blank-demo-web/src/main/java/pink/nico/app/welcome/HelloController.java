package pink.nico.app.welcome;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pink.nico.domain.model.Book;
import pink.nico.domain.service.BookService;

import javax.inject.Inject;

/**
 * Handles requests for the application home page.
 */
@Controller
@Slf4j
public class HelloController {

    @Inject
    BookService bookService;

    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
    public String home(Locale locale, Model model) {
        log.info("Welcome home! The client locale is {}.", locale);

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
                DateFormat.LONG, locale);

        String formattedDate = dateFormat.format(date);

        model.addAttribute("serverTime", formattedDate);

        return "welcome/home";
    }

    @GetMapping("/all")
    public String findAll(Model model){
        List<Book> books = bookService.findAll();

        model.addAttribute("message", "Hello, World!!!!!");
        model.addAttribute("books", books);

        return "welcome/home";
    }

}
