package pink.nico.domain.repository;

import org.apache.ibatis.annotations.Select;
import pink.nico.domain.model.Book;

import java.util.List;

/**
 * @author ykonno
 * @since 2017/04/20
 */
public interface BookRepository {

    @Select("SELECT * FROM book")
    List<Book> findAll();

    @Select("SELECT * FROM authors")
    List<Book> findAuthor();
}
