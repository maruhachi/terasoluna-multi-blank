package pink.nico.domain.service;

import pink.nico.domain.model.Book;

import java.util.List;

/**
 * @author ykonno
 * @since 2017/04/20
 *
 */
public interface BookService      {

    public List<Book> findAll();

    List<Book> findAuthor();
}
